var selected = new Map();
var matched = new Map();

function newCreateTable(countOfRows, countOfColumns){
	var newTable = document.createElement('table');
	var tableBoby = document.createElement('tbody');
	
	for (var i = 0; i < countOfRows; i++){
		var row = document.createElement('tr');
		for (var j = 0; j < countOfColumns; j++){
			var cell = document.createElement('td');
			row.appendChild(cell);
		}
		tableBoby.appendChild(row);
	}
	newTable.appendChild(tableBoby);
	
	return newTable;
}

function shafflePicture(pictures){
	var lambda = function(){return Math.random() - 0.5};
	pictures.sort(lambda);
}

function cellClick(event, numberArr, countOfRows, countOfColumns) {
	var cell = event.target;
	
	var c = cell.cellIndex;
	var r = cell.parentNode.rowIndex;
	var number = numberArr[r * countOfColumns + c];
	
	var key = r + "," + c;
	
	if (!matched.has(key)) {
		
		selected.set(key, {number, cell});
		cell.style.background = 'url(img/' + String(number) + '.jpg) no-repeat center center / contain';		
		
		if (selected.size == 2) {
			var keys = Array.from(selected.keys());
			var cell0 = selected.get(keys[0]);
			var cell1 = selected.get(keys[1]);

			setTimeout(function show() {
				if (cell0.number === cell1.number) {
					matched.set(keys[0], cell0);
					matched.set(keys[1], cell1);

					cell0.cell.style.visibility = 'hidden';
					cell1.cell.style.visibility = 'hidden';
					
					if (matched.size == countOfRows * countOfColumns) {
						alert("You win");
					}
				} else {
					cell0.cell.style.background = 'gray';
					cell1.cell.style.background = 'gray';
				}
			}, 1500);
			
			selected.clear();
		}
	}
}

function fillTable(table, countOfRows, countOfColumns){
	var uniquePicturesForTable = [];
	var countOfUniqueValues = (countOfColumns*countOfRows) / 2;

	var numberArr = [];
	for (var i = 1; i <= countOfUniqueValues; i++) {
		numberArr.push(i);
		numberArr.push(i);
	}
	shafflePicture(numberArr);
	var picArr = new Array();
	
	for (var i = 0; i < countOfRows; i++){
		for (var j = 0; j < countOfColumns; j++){
			var k = (i * countOfColumns) + j;
			table.rows[i].cells[j].style.background = 'gray';
			table.rows[i].cells[j].addEventListener('click', function(event) {
				cellClick(event, numberArr, countOfRows, countOfColumns);
			});
		}
	}
}

function newGameButtonClick(){
	var heightOfFieldStr = document.getElementById('x').value;
	var widthOfFieldStr = document.getElementById('y').value;
	var heightOfField = parseInt(heightOfFieldStr);
	var widthOfField = parseInt(widthOfFieldStr);
	
	if(heightOfFieldStr ==="" || widthOfFieldStr === ""){
		alert("X and Y must be filled");
	}
	else if(Number.isNaN(heightOfField) || Number.isNaN(widthOfField)){
		alert("X and Y must be numbers");
	}
	else if(heightOfField <= 0 || widthOfField <= 0){
		alert("X and Y must be positive numbers");
	}
	else if((heightOfField * widthOfField) % 2 != 0){
		alert("The multiplication X and Y must be an even number");
	}
	else if(heightOfField * widthOfField > 86){
		alert("The number of pictures must be less than 86");
	}
	else{
		selected = new Map();
		matched = new Map();
		
		var tableForField = newCreateTable(heightOfField, widthOfField);
		document.body.appendChild(tableForField);
		
		var result = document.getElementById('tablePrint');
		result.textContent = "";
		
		fillTable(tableForField, heightOfField, widthOfField);
		
		result.append(tableForField);	
	}
	
}